# bc_flutter_app

flutter run -d web-server --web-renderer canvaskit --web-hostname 0.0.0.0 --web-port 8989

# Build and deploy web app
flutter build web --release --web-renderer canvaskit
firebase deploy --only hosting
