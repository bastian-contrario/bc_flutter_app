import 'utils/utils.dart';

const _prodUrl = 'https://www.bastiancontrarioparma.it/wp-json/bc/v1';
const _debugUrl = _prodUrl;

final String apiUrl = (isInDebugMode) ? _debugUrl : _prodUrl;
const String phoneNumberLink = 'tel:+393478113440';
const String facebookPubPageUrl =
    'https://www.facebook.com/Bastian-Contrario-675650975896702/';
const String facebookTapRoomPageUrl =
    'https://www.facebook.com/people/Bastian-Contrario-Brewing-Company-Taproom-e-Produzione/100022757714967/';
const String instagramPubPageUrl =
    'https://www.instagram.com/bastiancontrarioparma/';
const String instagramTapRoomPageUrl =
    'https://www.instagram.com/bastiancontrariobrewingcompany/';
