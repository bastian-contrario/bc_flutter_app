import 'package:flutter/material.dart';

const kBaseStyle = TextStyle(
  fontSize: 14.0,
  fontWeight: FontWeight.w400,
  color: Colors.white,
);
