import 'package:flutter/foundation.dart';
import 'package:sentry/sentry.dart';

/// Report error to sentry.io project in production mode,
/// while print to console in debug mode.
Future<void> reportError(dynamic error, dynamic stackTrace) async {
  if (kDebugMode) {
    // Print the full stacktrace in debug mode.
    print('Caught error: $error, $stackTrace');
  } else {
    try {
      // In production mode report to Sentry.
      await Sentry.captureException(error, stackTrace: stackTrace);
    } catch (ex) {
      if (kDebugMode) {
        print('Sending report to sentry.io failed: $ex');
        print('Original error: $error');
      }
    }
  }
}
