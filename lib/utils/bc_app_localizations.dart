import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter/material.dart';

class BcAppLocalizations {
  final Locale locale;

  BcAppLocalizations(this.locale);

  static BcAppLocalizations? of(BuildContext context) {
    return Localizations.of<BcAppLocalizations>(context, BcAppLocalizations);
  }

  static final Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'error': 'Something went wrong',
      'netError':
          'Service temporarly not available. Check your connection and try again.',
      'eventsPageTitle': 'Events calendar',
      'tapListPageTitle': 'Tap list',
      'tvSchedulePageTitle': 'TV schedule',
      'menuPageTitle': 'Menu',
      'notificationsPageTitle': 'Notifications',
      'notificationsPageText': 'Get notified when following are updated:',
      'callUsActionTitle': 'Call us',
      'noTvSchedule': 'No TV schedule for now',
      'noMenu': 'No menu found',
      'noEvents': 'No events for now',
      'facebookPubActionTitle': 'Pub',
      'facebookTapRoomActionTitle': 'Brewing Company',
      'instagramPubActionTitle': 'Pub',
      'instagramTapRoomActionTitle': 'Brewing Company',
    },
    'it': {
      'error': 'Qualcosa è andato storto',
      'netError':
          'Servizio momentaneamento non disponibile. Controllare la connessione e riprovare.',
      'eventsPageTitle': 'Calendario eventi',
      'tapListPageTitle': 'Tap list',
      'tvSchedulePageTitle': 'Programmazione TV',
      'menuPageTitle': 'Menu',
      'notificationsPageTitle': 'Notifiche',
      'notificationsPageText': 'Ricevi una notifica quando vengono aggiornati:',
      'callUsActionTitle': 'Chiamaci',
      'noTvSchedule': 'Nessuna programmazione TV per ora',
      'noMenu': 'Nessun menu trovato',
      'noEvents': 'Nessun evento per ora',
      'facebookPubActionTitle': 'Pub',
      'facebookTapRoomActionTitle': 'Brewing Company',
      'instagramPubActionTitle': 'Pub',
      'instagramTapRoomActionTitle': 'Brewing Company',
    },
  };

  String? get error => _localizedValues[locale.languageCode]?['error'];
  String? get netError => _localizedValues[locale.languageCode]?['netError'];
  String? get eventsPageTitle =>
      _localizedValues[locale.languageCode]?['eventsPageTitle'];
  String? get tapListPageTitle =>
      _localizedValues[locale.languageCode]?['tapListPageTitle'];
  String? get tvSchedulePageTitle =>
      _localizedValues[locale.languageCode]?['tvSchedulePageTitle'];
  String? get menuPageTitle =>
      _localizedValues[locale.languageCode]?['menuPageTitle'];
  String? get notificationsPageTitle =>
      _localizedValues[locale.languageCode]?['notificationsPageTitle'];
  String? get notificationsPageText =>
      _localizedValues[locale.languageCode]?['notificationsPageText'];
  String? get callUsActionTitle =>
      _localizedValues[locale.languageCode]?['callUsActionTitle'];
  String? get noTvSchedule =>
      _localizedValues[locale.languageCode]?['noTvSchedule'];
  String? get noMenu =>
      _localizedValues[locale.languageCode]?['noMenu'];
  String? get noEvents =>
      _localizedValues[locale.languageCode]?['noEvents'];
  String? get facebookPubActionTitle =>
      _localizedValues[locale.languageCode]?['facebookPubActionTitle'];
  String? get facebookTapRoomActionTitle =>
      _localizedValues[locale.languageCode]?['facebookTapRoomActionTitle'];
  String? get instagramPubActionTitle =>
      _localizedValues[locale.languageCode]?['instagramPubActionTitle'];
  String? get instagramTapRoomActionTitle =>
      _localizedValues[locale.languageCode]?['instagramTapRoomActionTitle'];
}

class BcAppLocalizationsDelegate
    extends LocalizationsDelegate<BcAppLocalizations> {
  const BcAppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'it'].contains(locale.languageCode);

  @override
  Future<BcAppLocalizations> load(Locale locale) {
    return SynchronousFuture<BcAppLocalizations>(BcAppLocalizations(locale));
  }

  @override
  bool shouldReload(BcAppLocalizationsDelegate old) => false;
}
