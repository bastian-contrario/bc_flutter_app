import 'dart:io';

import 'package:bc_flutter_app/src/ui/tap_list_page/tap_list.dart';
import 'package:bc_flutter_app/src/ui/tap_list_page/tap_list_page.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import '../src/models/bc_location.dart';

class WidgetUtils {
  static Widget showStreamErrorMessage(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Text(
          message,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}

class PlatformLoader extends StatelessWidget {
  final Color? color;

  const PlatformLoader({this.color, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget loader;

    if (kIsWeb) {
      loader = CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color?>(
            (color != null) ? color : Theme.of(context).colorScheme.secondary),
      );
    } else if (Platform.isIOS) {
      loader = const CupertinoActivityIndicator();
    } else {
      loader = CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color?>(
            (color != null) ? color : Theme.of(context).colorScheme.secondary),
      );
    }

    return Center(
      child: loader,
    );
  }
}

/// A route with fade transition.
class FadeRoute extends PageRouteBuilder {
  final Widget page;
  @override
  final RouteSettings settings;

  FadeRoute({required this.page, required this.settings})
      : super(
          pageBuilder: (_, __, ___) => page,
          transitionsBuilder: (_, animation, __, child) => FadeTransition(
            opacity: animation,
            child: child,
          ),
        );

  static Route<dynamic> onGenerateRoute(
    RouteSettings settings,
    BuildContext context,
    Map<String, WidgetBuilder> routes,
  ) {
    if (settings.name == TapListPage.routeName) {
      final args = settings.arguments as Map<String, dynamic>;

      return FadeRoute(
        page: TapListPage(args['location'] as BcLocation),
        settings: settings,
      );
    }

    return FadeRoute(
      page: routes[settings.name]!(context),
      settings: settings,
    );
  }
}
