import 'package:bc_flutter_app/src/models/bc_location.dart';
import 'package:bc_flutter_app/src/ui/menu_page/menu_page.dart';
import 'package:bc_flutter_app/src/ui/notifications_page/notifications_page.dart';
import 'package:bc_flutter_app/src/ui/tap_list_page/tap_list_page.dart';
import 'package:bc_flutter_app/src/ui/tv_schedule_page/tv_schedule_page.dart';
import 'package:flutter/widgets.dart';

import 'src/ui/events_page/events_page.dart';
import 'src/ui/home_page/home_page.dart';

Map<String, WidgetBuilder> routes = {
  HomePage.routeName: (context) => const HomePage(),
  EventsPage.routeName: (context) => const EventsPage(),
  // TapListPage.routeName: (context) {
  //   final args =
  //       (ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>);
  //   print(args);
  //   return TapListPage(args['location'] as BcLocation);
  // },
  TvSchedulePage.routeName: (context) => const TvSchedulePage(),
  MenuPage.routeName: (context) => const MenuPage(),
  NotificationsPage.routeName: (context) => const NotificationsPage(),
};
