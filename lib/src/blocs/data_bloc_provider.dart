import 'package:flutter/material.dart';
import 'data_bloc.dart';

export 'data_bloc.dart';

class DataBlocProvider extends InheritedWidget {
  final bloc = DataBloc();

  DataBlocProvider({Key? key, required Widget child})
      : super(key: key, child: child) {
    // bloc.getData();
    bloc.getTapList();
  }

  @override
  bool updateShouldNotify(oldWidget) => true;

  static DataBloc? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<DataBlocProvider>()?.bloc;
  }
}
