import 'dart:io';

import 'package:bc_flutter_app/src/models/event.dart';
import 'package:bc_flutter_app/src/models/menu.dart';
import 'package:bc_flutter_app/src/models/tap.dart';
import 'package:bc_flutter_app/src/models/tv_schedule.dart';
import 'package:bc_flutter_app/src/resources/repository.dart';
import 'package:bc_flutter_app/utils/error_reporting.dart';
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:rxdart/rxdart.dart';

class DataBloc {
  final _repository = Repository();
  final _eventsFetcher = PublishSubject<List<Event>>();
  final _taps = BehaviorSubject<List<Tap>>();
  final _tvScheduleFetcher = PublishSubject<TvSchedule>();
  final _menu = BehaviorSubject<Menu>();

  Stream<List<Event>> get events => _eventsFetcher.stream;
  Stream<List<Tap>> get taps => _taps.stream;
  Stream<TvSchedule> get tvSchedule => _tvScheduleFetcher.stream;
  Stream<Menu> get menu => _menu.stream;

  Menu get menuValue =>
      Menu('https://bastiancontrarioparma.it/wp-content/uploads/menu.pdf');

  Future<File> getFilePath(String url) async {
    final filename = url.substring(url.lastIndexOf("/") + 1);
    var request = await HttpClient().getUrl(Uri.parse(url));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = File('$dir/$filename');
    await file.writeAsBytes(bytes);
    return file;
  }

  Future<void> getTapList() async {
    try {
      final tapList = await _repository.getTapList();
      final Map<String, Tap> starredTaps = await _repository.getStarredTaps();
      for (String id in starredTaps.keys) {
        tapList.firstWhere((tap) => tap.id == id).starred = true;
      }
      tapList.sort((a, b) => (a.starred) ? -1 : 1);
      _taps.sink.add(tapList);
    } catch (ex, stackTrace) {
      _eventsFetcher.sink.addError(Error());
      _taps.sink.addError(Error());
      _tvScheduleFetcher.sink.addError(Error());
      _menu.sink.addError(Error());
      reportError(ex, stackTrace);
    }
  }

  Future<void> getData() async {
    try {
      Map<String, dynamic>? data = await _repository.getData();

      if (data == null) {
        throw Error();
      }

      _eventsFetcher.sink.add((data['events'] as List<dynamic>)
          .map((event) => Event.fromMap(event))
          .toList());

      final taps = (data['tap_beers'] as List<dynamic>)
          .map((tap) => Tap.fromMap(tap))
          .toList();
      final Map<String, Tap> starredTaps = await _repository.getStarredTaps();
      for (String id in starredTaps.keys) {
        taps.firstWhere((tap) => tap.id == id).starred = true;
      }
      taps.sort((a, b) => (a.starred) ? -1 : 1);
      _taps.sink.add(taps);

      _tvScheduleFetcher.sink.add(TvSchedule(data['tv_blackboard_img_url']));

      _menu.sink.add(Menu(data['menu_pdf_url']));
    } catch (ex, stackTrace) {
      _eventsFetcher.sink.addError(Error());
      _taps.sink.addError(Error());
      _tvScheduleFetcher.sink.addError(Error());
      _menu.sink.addError(Error());
      reportError(ex, stackTrace);
    }
  }

  void toggleStarredTap(String tapId) async {
    final taps = _taps.value;
    final tap = taps.firstWhere((tap) => tap.id == tapId);
    tap.starred = !tap.starred;

    if (tap.starred) {
      await _repository.setStarredTap(tap);
    } else {
      await _repository.removeStarredTap(tap.id);
    }

    taps.sort((a, b) => (a.starred) ? -1 : 1);
    _taps.sink.add(taps);
  }

  void dispose() async {
    await _eventsFetcher.drain();
    _eventsFetcher.close();
    await _taps.drain();
    _taps.close();
    await _tvScheduleFetcher.drain();
    _tvScheduleFetcher.close();
    await _menu.drain();
    _menu.close();
  }
}
