import 'package:flutter/material.dart';
import 'notification_bloc.dart';

export 'notification_bloc.dart';

class NotificationBlocProvider extends InheritedWidget {
  final bloc = NotificationBloc();

  NotificationBlocProvider({Key? key, required Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(oldWidget) => true;

  static NotificationBloc? of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<NotificationBlocProvider>()
        ?.bloc;
  }
}
