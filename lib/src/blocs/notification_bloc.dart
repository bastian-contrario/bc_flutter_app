import 'dart:async';

import 'package:bc_flutter_app/utils/error_reporting.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotificationBloc {
  static final topicKeys = ['tv', 'tap-list', 'events'];
  final FirebaseMessaging _fcm = FirebaseMessaging.instance;
  final _topics = BehaviorSubject<Map<String, bool>>.seeded(
      {for (var topic in topicKeys) topic: true});

  Stream<Map<String, bool>> get topics => _topics.stream;
  Future<String?> get fcmToken => _fcm.getToken(
        vapidKey:
            'BKA0Zz8k2lBU3kcUwRikLGyrHFAIunhpvh5933kxR21c0FDDYQ45P_RlNWlnYWVEg9ZBXJV_3wlTBI031Vg6g8A',
      );

  Future<void> init() async {
    try {
      FirebaseMessaging.onMessage.listen((event) {
        _onMessage(event.data);
      });

      FirebaseMessaging.onMessageOpenedApp.listen((event) {
        _onLaunch(event.data);
      });

      await _fcm.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true,
      );
      _initTopics();
    } catch (ex, stackTrace) {
      reportError(ex, stackTrace);
    }
  }

  void _initTopics() async {
    final _topicValues = _topics.value;

    try {
      final prefs = await SharedPreferences.getInstance();
      for (String topic in topicKeys) {
        final subscribed = prefs.getBool(topic);
        if (subscribed == null) {
          if (!kIsWeb) {
            await _fcm.subscribeToTopic(topic);
          }
          prefs.setBool(topic, true);
          _topicValues[topic] = true;
        } else {
          _topicValues[topic] = subscribed;
        }
      }
      _topics.sink.add(_topicValues);
    } catch (ex, stackTrace) {
      reportError(ex, stackTrace);
    }
  }

  Future<dynamic> _onMessage(Map<String, dynamic> message) async {
    if (kDebugMode) {
      print('onMessage: $message');
    }
  }

  Future<dynamic> _onLaunch(Map<String, dynamic> message) async {
    if (kDebugMode) {
      print('onLaunch: $message');
    }
  }

  void subscribeToTopic(String topic) async {
    try {
      if (!kIsWeb) {
        await _fcm.subscribeToTopic(topic);
      }
      final prefs = await SharedPreferences.getInstance();
      prefs.setBool(topic, true);
      Map<String, bool> _topicValues = _topics.value;
      _topicValues[topic] = true;
      _topics.sink.add(_topicValues);
    } catch (ex, stackTrace) {
      reportError(ex, stackTrace);
    }
  }

  void unsubscribeFromTopic(String topic) async {
    try {
      await _fcm.unsubscribeFromTopic(topic);
      final prefs = await SharedPreferences.getInstance();
      prefs.setBool(topic, false);
      Map<String, bool> _topicValues = _topics.value;
      _topicValues[topic] = false;
      _topics.sink.add(_topicValues);
    } catch (ex, stackTrace) {
      reportError(ex, stackTrace);
    }
  }

  void dispose() async {
    await _topics.drain();
    _topics.close();
  }
}
