class Tap {
  late String id;
  late String alcohol;
  late String brewery;
  late String city;
  late String color;
  late String name;
  late String type;
  bool starred = false;
  late String location;

  Tap();

  Tap.fromMap(Map<String, dynamic> parsedJson) {
    id = parsedJson['name'].toLowerCase().trim().replaceAll(RegExp(r' '), '-');
    alcohol = parsedJson['alcohol'].trim();
    brewery = parsedJson['brewery'].trim();
    city = parsedJson['city'].trim();
    color = parsedJson['color'].trim();
    name = parsedJson['name'].trim();
    type = parsedJson['type'].trim();
    location = parsedJson['location'].trim();
  }

  Map<String, dynamic> toMap() => {
        'name': name,
        'alcohol': alcohol,
        'brewery': brewery,
        'city': city,
        'color': color,
        'type': type,
        'location': location,
      };
}
