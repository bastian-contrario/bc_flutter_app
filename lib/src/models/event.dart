class Event {
  late DateTime date;
  late String name;
  late String description;
  late String imageUrl;

  Event();

  Event.fromMap(Map<String, dynamic> parsedJson) {
    date = DateTime.parse(parsedJson['date']);
    name = parsedJson['name'];
    description = parsedJson['description'];
    imageUrl = parsedJson['img_url'];
  }
}
