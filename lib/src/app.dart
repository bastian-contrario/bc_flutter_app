import 'package:bc_flutter_app/utils/bc_app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import '../utils/bc_app_localizations.dart';
import '../utils/widget_utils.dart';
import '../router.dart';
import 'blocs/data_bloc_provider.dart';
import 'blocs/notification_bloc_provider.dart';

class BcApp extends StatelessWidget {
  const BcApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NotificationBlocProvider(
      child: DataBlocProvider(
        child: MaterialApp(
          localizationsDelegates: const [
            BcAppLocalizationsDelegate(),
            GlobalMaterialLocalizations.delegate,
          ],
          theme: ThemeData(
            fontFamily: 'Roboto',
            colorScheme: const ColorScheme.dark(),
          ),
          supportedLocales: const [
            Locale('en'),
            Locale('it'),
          ],
          onGenerateRoute: (RouteSettings settings) =>
              FadeRoute.onGenerateRoute(settings, context, routes),
        ),
      ),
    );
  }
}
