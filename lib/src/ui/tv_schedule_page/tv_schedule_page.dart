import 'package:bc_flutter_app/src/blocs/data_bloc_provider.dart';
import 'package:bc_flutter_app/src/models/tv_schedule.dart';
import 'package:bc_flutter_app/src/ui/background_image.dart';
import 'package:bc_flutter_app/utils/bc_app_localizations.dart';
import 'package:bc_flutter_app/utils/widget_utils.dart';
import 'package:flutter/material.dart';

class TvSchedulePage extends StatefulWidget {
  static String get routeName => '/tvSchedule';

  const TvSchedulePage({Key? key}) : super(key: key);

  @override
  _TvSchedulePageState createState() => _TvSchedulePageState();
}

class _TvSchedulePageState extends State<TvSchedulePage> {
  DataBloc? _dataBloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _dataBloc = DataBlocProvider.of(context);
    _dataBloc?.getData();
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundImage(
      'assets/images/bg5.jpg',
      child: Scaffold(
        appBar: AppBar(
          title: Text(
              BcAppLocalizations.of(context)!.tvSchedulePageTitle.toString()),
          backgroundColor: Colors.transparent,
        ),
        backgroundColor: Colors.transparent.withAlpha(127),
        body: StreamBuilder<TvSchedule>(
          stream: _dataBloc!.tvSchedule,
          builder: (BuildContext context, AsyncSnapshot<TvSchedule> snapshot) {
            if (snapshot.hasError) {
              WidgetUtils.showStreamErrorMessage(snapshot.error.toString());
            }

            if (!snapshot.hasData) {
              return const PlatformLoader();
            }

            return Center(
              child: (snapshot.data!.imageUrl != '')
                  ? Image.network(snapshot.data!.imageUrl)
                  : Text(
                      BcAppLocalizations.of(context)!.noTvSchedule.toString()),
            );
          },
        ),
      ),
    );
  }
}
