import 'package:bc_flutter_app/src/blocs/data_bloc_provider.dart';
import 'package:bc_flutter_app/src/models/menu.dart';
import 'package:bc_flutter_app/utils/bc_app_localizations.dart';
import 'package:bc_flutter_app/utils/widget_utils.dart';
import 'package:flutter/material.dart';
import 'package:internet_file/internet_file.dart';
import 'package:pdfx/pdfx.dart';

import '../background_image.dart';

class MenuPage extends StatefulWidget {
  static String get routeName => '/menu';

  const MenuPage({Key? key}) : super(key: key);

  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  DataBloc? _dataBloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _dataBloc = DataBlocProvider.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundImage(
      'assets/images/bg5.jpg',
      child: Scaffold(
        appBar: AppBar(
          title: Text(BcAppLocalizations.of(context)!.menuPageTitle.toString()),
          backgroundColor: Colors.transparent,
        ),
        backgroundColor: Colors.transparent.withAlpha(127),
        body: PdfView(
          controller: PdfController(
            document: PdfDocument.openData(
                InternetFile.get(_dataBloc!.menuValue.url)),
          ),
        ),
      ),
    );
  }
}
