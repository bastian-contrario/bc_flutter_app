import 'package:flutter/material.dart';

class BackgroundImage extends StatelessWidget {
  final Widget? child;
  final String assetUrl;

  const BackgroundImage(this.assetUrl, {Key? key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(assetUrl),
          fit: BoxFit.cover,
        ),
      ),
      child: (child != null) ? child : Container(),
    );
  }
}
