import 'package:bc_flutter_app/src/blocs/data_bloc_provider.dart';
import 'package:bc_flutter_app/src/models/tap.dart';
import 'package:bc_flutter_app/src/ui/tap_list_page/tap_tile.dart';
import 'package:bc_flutter_app/utils/bc_app_localizations.dart';
import 'package:bc_flutter_app/utils/widget_utils.dart';
import 'package:flutter/material.dart';

import '../../models/bc_location.dart';
import '../background_image.dart';

class TapList extends StatefulWidget {
  final BcLocation location;

  const TapList(this.location, {Key? key}) : super(key: key);

  @override
  _TapListState createState() => _TapListState();
}

class _TapListState extends State<TapList> {
  DataBloc? _dataBloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _dataBloc = DataBlocProvider.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundImage(
      'assets/images/bg5.jpg',
      child: Scaffold(
        appBar: AppBar(
          title:
              Text(BcAppLocalizations.of(context)!.tapListPageTitle.toString()),
          backgroundColor: Colors.transparent,
        ),
        backgroundColor: Colors.transparent.withAlpha(127),
        body: StreamBuilder<List<Tap>>(
          stream: _dataBloc!.taps,
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              WidgetUtils.showStreamErrorMessage(snapshot.error.toString());
            }

            if (!snapshot.hasData) {
              return const PlatformLoader();
            }

            final taps = snapshot.data!
                .where((beer) => beer.location == widget.location.name)
                .toList();

            return ListView.separated(
              separatorBuilder: (context, index) => const Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.0),
                child: Divider(
                  color: Colors.white70,
                ),
              ),
              itemCount: taps.length,
              itemBuilder: (context, index) {
                return TapTile(taps[index]);
              },
            );
          },
        ),
      ),
    );
  }
}
