import 'package:bc_flutter_app/src/blocs/data_bloc_provider.dart';
import 'package:bc_flutter_app/src/models/tap.dart';
import 'package:flutter/material.dart';

import '../../../utils/utils.dart' show StringExtension;

class TapTile extends StatelessWidget {
  final Tap tap;

  const TapTile(this.tap, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: ListTile(
        leading: Image.asset('assets/images/${tap.color}.png'),
        title: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.all(3.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(3),
              ),
              child: Text(tap.type.capitalize(),
                  style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 12.0,
                  )),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 5.0),
              child: Text(
                tap.name.capitalize(),
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,
                ),
              ),
            ),
            Text(
              tap.brewery.capitalize(),
              style: const TextStyle(
                color: Colors.white70,
                fontWeight: FontWeight.bold,
                fontSize: 13.0,
              ),
            ),
          ],
        ),
        trailing: Column(
          children: [
            Text(
              tap.alcohol,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12.0,
              ),
            ),
            Expanded(
              child: IconButton(
                icon: (tap.starred)
                    ? const Icon(Icons.star, color: Colors.yellow)
                    : const Icon(Icons.star_border),
                onPressed: () {
                  DataBlocProvider.of(context)!.toggleStarredTap(tap.id);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
