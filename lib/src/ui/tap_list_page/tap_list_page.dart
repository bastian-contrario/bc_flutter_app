import 'package:bc_flutter_app/src/models/bc_location.dart';
import 'package:flutter/material.dart';

import 'tap_list.dart';

class TapListPage extends StatelessWidget {
  final BcLocation location;

  const TapListPage(this.location, {Key? key}) : super(key: key);

  static String get routeName => '/tapList';

  @override
  Widget build(BuildContext context) {
    return TapList(location);
  }
}
