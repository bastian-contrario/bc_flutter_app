import 'package:bc_flutter_app/src/blocs/data_bloc_provider.dart';
import 'package:bc_flutter_app/src/models/event.dart';
import 'package:bc_flutter_app/utils/bc_app_localizations.dart';
import 'package:bc_flutter_app/utils/widget_utils.dart';
import 'package:flutter/material.dart';

import '../background_image.dart';

class EventsPage extends StatefulWidget {
  static String get routeName => '/events';

  const EventsPage({Key? key}) : super(key: key);

  @override
  _EventsPageState createState() => _EventsPageState();
}

class _EventsPageState extends State<EventsPage> {
  DataBloc? _dataBloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _dataBloc = DataBlocProvider.of(context);
    _dataBloc?.getData();
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundImage(
      'assets/images/bg5.jpg',
      child: Scaffold(
        appBar: AppBar(
          title:
              Text(BcAppLocalizations.of(context)!.eventsPageTitle.toString()),
          backgroundColor: Colors.transparent,
        ),
        backgroundColor: Colors.transparent.withAlpha(127),
        body: StreamBuilder<List<Event>>(
          stream: _dataBloc!.events,
          builder: (BuildContext context, AsyncSnapshot<List<Event>> snapshot) {
            if (snapshot.hasError) {
              WidgetUtils.showStreamErrorMessage(snapshot.error.toString());
            }

            if (!snapshot.hasData) {
              return const PlatformLoader();
            }

            return Center(
              child: Text(BcAppLocalizations.of(context)!.noEvents.toString()),
            );
          },
        ),
      ),
    );
  }
}
