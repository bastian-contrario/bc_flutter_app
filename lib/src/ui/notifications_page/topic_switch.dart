import 'package:bc_flutter_app/src/blocs/notification_bloc_provider.dart';
import 'package:flutter/material.dart';

class TopicSwitch extends StatefulWidget {
  final String title;
  final String topic;

  const TopicSwitch({Key? key, required this.title, required this.topic})
      : super(key: key);

  @override
  _TopicSwitchState createState() => _TopicSwitchState();
}

class _TopicSwitchState extends State<TopicSwitch> {
  NotificationBloc? _notificationBloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _notificationBloc = NotificationBlocProvider.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(widget.title),
      trailing: StreamBuilder<Map<String, bool>>(
        stream: _notificationBloc!.topics,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const SizedBox();
          }

          return Switch(
            value: snapshot.data![widget.topic] ?? false,
            onChanged: (value) {
              if (value) {
                _notificationBloc!.subscribeToTopic(widget.topic);
              } else {
                _notificationBloc!.unsubscribeFromTopic(widget.topic);
              }
            },
            //activeTrackColor: Colors.lightGreenAccent,
            //activeColor: Colors.green,
          );
        },
      ),
    );
  }
}
