import 'package:bc_flutter_app/src/ui/notifications_page/topic_switch.dart';
import 'package:bc_flutter_app/utils/bc_app_localizations.dart';
import 'package:flutter/material.dart';

import '../background_image.dart';

class NotificationsPage extends StatelessWidget {
  static String get routeName => '/notifications';

  const NotificationsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BackgroundImage(
      'assets/images/bg5.jpg',
      child: Scaffold(
        appBar: AppBar(
          title: Text(BcAppLocalizations.of(context)!
              .notificationsPageTitle
              .toString()),
          backgroundColor: Colors.transparent,
        ),
        backgroundColor: Colors.transparent.withAlpha(127),
        body: Padding(
          padding: const EdgeInsets.all(25.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Text(
                  BcAppLocalizations.of(context)!
                      .notificationsPageText
                      .toString()
                      .toUpperCase(),
                  style: const TextStyle(
                    fontSize: 12.0,
                  ),
                ),
              ),
              TopicSwitch(
                title:
                    BcAppLocalizations.of(context)!.tapListPageTitle.toString(),
                topic: 'tap-list',
              ),
              TopicSwitch(
                title:
                    BcAppLocalizations.of(context)!.eventsPageTitle.toString(),
                topic: 'events',
              ),
              TopicSwitch(
                title: BcAppLocalizations.of(context)!
                    .tvSchedulePageTitle
                    .toString(),
                topic: 'tv',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
