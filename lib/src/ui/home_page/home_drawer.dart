import 'package:bc_flutter_app/settings.dart';
import 'package:bc_flutter_app/utils/error_reporting.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:bc_flutter_app/src/ui/notifications_page/notifications_page.dart';
import 'package:bc_flutter_app/utils/bc_app_localizations.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeDrawer extends StatelessWidget {
  const HomeDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: Colors.black,
      ),
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            const SizedBox(
              height: 40.0,
            ),
            (!kIsWeb)
                ? ListTile(
                    title: Text(BcAppLocalizations.of(context)!
                        .notificationsPageTitle
                        .toString()),
                    leading: const Icon(Icons.notifications),
                    onTap: () {
                      Navigator.of(context)
                          .pushNamed(NotificationsPage.routeName);
                    },
                  )
                : Container(),
            ListTile(
              title: Text(
                  BcAppLocalizations.of(context)!.callUsActionTitle.toString()),
              leading: const Icon(Icons.call),
              onTap: () async {
                try {
                  if (await canLaunch(phoneNumberLink)) {
                    await launch(phoneNumberLink);
                  }
                } catch (ex, stackTrace) {
                  reportError(ex, stackTrace);
                }
              },
            ),
            ListTile(
              title: Text(BcAppLocalizations.of(context)!
                  .facebookPubActionTitle
                  .toString()),
              leading: const FaIcon(FontAwesomeIcons.facebook),
              onTap: () async {
                try {
                  if (await canLaunch(facebookPubPageUrl)) {
                    await launch(facebookPubPageUrl);
                  }
                } catch (ex, stackTrace) {
                  reportError(ex, stackTrace);
                }
              },
            ),
            ListTile(
              title: Text(BcAppLocalizations.of(context)!
                  .facebookTapRoomActionTitle
                  .toString()),
              leading: const FaIcon(FontAwesomeIcons.facebook),
              onTap: () async {
                try {
                  if (await canLaunch(facebookTapRoomPageUrl)) {
                    await launch(facebookTapRoomPageUrl);
                  }
                } catch (ex, stackTrace) {
                  reportError(ex, stackTrace);
                }
              },
            ),
            ListTile(
              title: Text(BcAppLocalizations.of(context)!
                  .instagramPubActionTitle
                  .toString()),
              leading: const FaIcon(FontAwesomeIcons.instagram),
              onTap: () async {
                try {
                  if (await canLaunch(instagramPubPageUrl)) {
                    await launch(instagramPubPageUrl);
                  }
                } catch (ex, stackTrace) {
                  reportError(ex, stackTrace);
                }
              },
            ),
            ListTile(
              title: Text(BcAppLocalizations.of(context)!
                  .instagramTapRoomActionTitle
                  .toString()),
              leading: const FaIcon(FontAwesomeIcons.instagram),
              onTap: () async {
                try {
                  if (await canLaunch(instagramTapRoomPageUrl)) {
                    await launch(instagramTapRoomPageUrl);
                  }
                } catch (ex, stackTrace) {
                  reportError(ex, stackTrace);
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
