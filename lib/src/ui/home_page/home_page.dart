import 'package:bc_flutter_app/src/blocs/notification_bloc_provider.dart';
import 'package:bc_flutter_app/src/models/bc_location.dart';
import 'package:bc_flutter_app/src/ui/background_image.dart';
import 'package:bc_flutter_app/src/ui/events_page/events_page.dart';
import 'package:bc_flutter_app/src/ui/home_page/home_drawer.dart';
import 'package:bc_flutter_app/src/ui/menu_page/menu_page.dart';
import 'package:bc_flutter_app/src/ui/tap_list_page/tap_list_page.dart';
import 'package:bc_flutter_app/utils/bc_app_localizations.dart';
import 'package:bc_flutter_app/utils/constants.dart';
import 'package:bc_flutter_app/utils/utils.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../settings.dart';
import '../../blocs/data_bloc_provider.dart';

class HomePage extends StatefulWidget {
  static String get routeName => '/';

  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  NotificationBloc? _notificationBloc;
  DataBloc? _dataBloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _notificationBloc = NotificationBlocProvider.of(context);
    _notificationBloc?.init();
    _dataBloc = DataBlocProvider.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundImage(
      'assets/images/bg5.jpg',
      child: Scaffold(
        drawer: const HomeDrawer(),
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: const Text('Bastian Contrario'),
          backgroundColor: Colors.transparent,
          elevation: 0.0,
        ),
        body: Column(
          children: [
            // _pageLink(
            //   context,
            //   routeName: EventsPage.routeName,
            //   imageAssetUrl: 'assets/images/eventi.png',
            //   pageTitle:
            //       BcAppLocalizations.of(context)!.eventsPageTitle.toString(),
            // ),
            // _divider(),
            _pageLink(
              context,
              routeName: TapListPage.routeName,
              imageAssetUrl: 'assets/images/spine.png',
              pageTitle:
                  '${BcAppLocalizations.of(context)!.tapListPageTitle.toString()} - Pub',
              arguments: {
                'location': BcLocation.pub,
              },
            ),
            _divider(),
            _pageLink(context,
                routeName: TapListPage.routeName,
                imageAssetUrl: 'assets/images/spine.png',
                pageTitle:
                    '${BcAppLocalizations.of(context)!.tapListPageTitle.toString()} - Birrificio',
                arguments: {
                  'location': BcLocation.birrificio,
                }),
            _divider(),
            _pageLink(
              context,
              routeName: MenuPage.routeName,
              imageAssetUrl: 'assets/images/menu.png',
              pageTitle:
                  BcAppLocalizations.of(context)!.menuPageTitle.toString(),
            ),
            _divider(),
            Expanded(
              child: Row(
                children: [
                  _externalLink(
                    context,
                    url: facebookPubPageUrl,
                    icon: const FaIcon(FontAwesomeIcons.facebook),
                    pageTitle: BcAppLocalizations.of(context)!
                        .facebookPubActionTitle
                        .toString(),
                  ),
                  _externalLink(
                    context,
                    url: facebookTapRoomPageUrl,
                    icon: const FaIcon(FontAwesomeIcons.facebook),
                    pageTitle: BcAppLocalizations.of(context)!
                        .facebookTapRoomActionTitle
                        .toString(),
                  ),
                  _externalLink(
                    context,
                    url: instagramPubPageUrl,
                    icon: const FaIcon(FontAwesomeIcons.instagram),
                    pageTitle: BcAppLocalizations.of(context)!
                        .instagramPubActionTitle
                        .toString(),
                  ),
                  _externalLink(
                    context,
                    url: instagramTapRoomPageUrl,
                    icon: const FaIcon(FontAwesomeIcons.instagram),
                    pageTitle: BcAppLocalizations.of(context)!
                        .instagramTapRoomActionTitle
                        .toString(),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _divider() => (kIsWeb)
      ? Container()
      : const Divider(
          color: Colors.white54,
        );

  Widget _pageLink(
    BuildContext context, {
    required String routeName,
    required String imageAssetUrl,
    required String pageTitle,
    Object? arguments,
  }) =>
      Expanded(
        child: InkWell(
          onTap: () async {
            if (pageTitle ==
                    BcAppLocalizations.of(context)!.menuPageTitle.toString() &&
                kIsWeb) {
              await _dataBloc!.getData();
              launchUrl(_dataBloc!.menuValue.url);
            } else {
              Navigator.of(context).pushNamed(
                routeName,
                arguments: arguments,
              );
            }
          },
          child: Row(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      imageAssetUrl,
                      height: ScreenSize(context).screenWidth * 0.15,
                      width: ScreenSize(context).screenWidth * 0.15,
                    ),
                    Text(
                      pageTitle,
                      style: kBaseStyle,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );

  Widget _externalLink(
    BuildContext context, {
    required String url,
    required Widget icon,
    required String pageTitle,
  }) =>
      Expanded(
        child: InkWell(
          onTap: () async {
            launchUrl(url);
          },
          child: Row(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    icon,
                    Text(
                      pageTitle,
                      style: kBaseStyle,
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
}
