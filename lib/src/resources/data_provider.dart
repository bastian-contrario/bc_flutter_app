import 'dart:convert';

import 'package:bc_flutter_app/src/models/tap.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../settings.dart';

class DataProvider {
  final _client = Client();

  Future<List<Tap>> getTapList() async {
    final url = '$apiUrl/tap-list';
    final response = await _client.get(Uri.parse(url));
    final tapList = <Tap>[];

    if (response.statusCode != 200) {
      throw Error();
    }

    final data = json.decode(response.body);

    for (final Map<String, dynamic> beer in data) {
      tapList.add(Tap.fromMap(beer));
    }

    return tapList;
  }

  Future<Map<String, dynamic>?> getData() async {
    final url = '$apiUrl/get_data_v2/';
    final response = await _client.get(Uri.parse(url));

    if (response.statusCode != 200) {
      return null;
    }

    final Map<String, dynamic> data = json.decode(response.body);
    return data;
  }

  Future<Map<String, Tap>> getStarredTaps() async {
    final prefs = await SharedPreferences.getInstance();
    final Map<String, dynamic> storedStarredTaps =
        json.decode(prefs.getString('starredTaps') ?? '{}');
    final Map<String, Tap> starredTaps = {};

    for (final String key in storedStarredTaps.keys) {
      starredTaps[key] = Tap.fromMap(json.decode(storedStarredTaps[key]));
    }

    return starredTaps;
  }

  Future<bool> setStarredTap(Tap tap) async {
    final prefs = await SharedPreferences.getInstance();
    final Map<String, dynamic> starredTaps =
        json.decode(prefs.getString('starredTaps') ?? '{}');
    starredTaps[tap.id] = json.encode(tap.toMap());
    return await prefs.setString('starredTaps', json.encode(starredTaps));
  }

  Future<bool> removeStarredTap(String tapId) async {
    final prefs = await SharedPreferences.getInstance();
    final Map<String, dynamic> starredTaps =
        json.decode(prefs.getString('starredTaps') ?? '{}');
    starredTaps.remove(tapId);
    return await prefs.setString('starredTaps', json.encode(starredTaps));
  }
}
