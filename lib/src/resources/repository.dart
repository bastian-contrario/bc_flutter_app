import 'package:bc_flutter_app/src/models/tap.dart';
import 'package:bc_flutter_app/src/resources/data_provider.dart';

class Repository {
  final _dataProvider = DataProvider();

  Future<Map<String, dynamic>?> getData() => _dataProvider.getData();
  Future<List<Tap>> getTapList() => _dataProvider.getTapList();
  Future<Map<String, Tap>> getStarredTaps() => _dataProvider.getStarredTaps();
  Future<bool> setStarredTap(Tap tap) => _dataProvider.setStarredTap(tap);
  Future<bool> removeStarredTap(String tapId) =>
      _dataProvider.removeStarredTap(tapId);
}
