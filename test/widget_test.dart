import 'package:flutter_test/flutter_test.dart';

import 'package:bc_flutter_app/src/app.dart';

void main() {
  testWidgets('App starts', (WidgetTester tester) async {
    await tester.pumpWidget(const BcApp());
  });
}
